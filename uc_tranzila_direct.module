<?php

// $Id: uc_tranzila_direct.module

/**
 * @file
 * Integrates tranzila.co.il's redirected payment service. Based on the 2Checkout UC module.
 */

/**
 * Implementation of hook_menu().
 */
function uc_tranzila_direct_menu() {
  $items = array();

  $items['cart/tranzila_direct/complete'] = array(
      'title' => 'Order complete',
      'page callback' => 'uc_tranzila_direct_complete',
      'access callback' => 'uc_tranzila_direct_callbacks_access',
      'type' => MENU_CALLBACK,
  );

  $items['cart/tranzila_direct/failed'] = array(
      'title' => 'Order rejected',
      'page callback' => 'uc_tranzila_direct_failed',
      'access callback' => 'uc_tranzila_direct_callbacks_access',
      'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Make sure anyone can complete their tranzila direct orders.
 */
function uc_tranzila_direct_callbacks_access() {
  return TRUE;
}

/**
 * Implementation of hook_init().
 */
function uc_tranzila_direct_init() {
  global $conf;
  $conf['i18n_variables'][] = 'uc_tranzila_direct_method_title';
  $conf['i18n_variables'][] = 'uc_tranzila_direct_checkout_button';
}

/**
 * Implementation of hook_ucga_display().
 */
function uc_tranzila_direct_ucga_display() {
  // Tell UC Google Analytics to display the e-commerce JS on the custom
  // order completion page for this module.
  if (arg(0) == 'cart' && arg(1) == 'tranzila_direct' && arg(2) == 'complete') {
    return TRUE;
  }
}

/**
 * Implementation of hook_payment_method().
 *
 * @see uc_payment_method_tranzila_direct()
 */
function uc_tranzila_direct_payment_method() {
  $path = base_path() . drupal_get_path('module', 'uc_tranzila_direct');
  $title = variable_get('uc_tranzila_direct_method_title', t('Credit card on a secure server provided by Tranzila'));
  /* $title .= '<br /><img src="'. $path .'/2cocc05.gif" style="position: relative; left: 2.5em;" />'; */

  $methods[] = array(
      'id' => 'tranzila_direct',
      'name' => t('Tranzila Direct'),
      'title' => $title,
      'desc' => t('Redirect to Tranzila Direct to pay by credit card.'),
      'callback' => 'uc_payment_method_tranzila_direct',
      'weight' => 3,
      'checkout' => TRUE,
      'no_gateway' => TRUE,
  );

  return $methods;
}

/**
 * Add Tranzila Direct settings to the payment method settings form.
 *
 * @see uc_tranzila_direct_payment_method()
 */
function uc_payment_method_tranzila_direct($op, &$arg1) {
  switch ($op) {

    case 'settings':
      $form['uc_tranzila_direct_terminal_name'] = array(
          '#type' => 'textfield',
          '#title' => t('Vendor terminal name'),
          '#description' => t('Your Tranzila vendor terminal name.'),
          '#default_value' => variable_get('uc_tranzila_direct_terminal_name', ''),
          '#size' => 32,
      );
      $form['uc_tranzila_direct_method_title'] = array(
          '#type' => 'textfield',
          '#title' => t('Payment method title'),
          '#default_value' => variable_get('uc_tranzila_direct_method_title', t('Credit card on a secure server provided by Tranzila')),
      );
      $form['uc_tranzila_direct_checkout_button'] = array(
          '#type' => 'textfield',
          '#title' => t('Order review submit button text'),
          '#description' => t('Provide Tranzila Direct specific text for the submit button on the order review page.'),
          '#default_value' => variable_get('uc_tranzila_direct_checkout_button', t('Submit Order')),
      );
      $form['uc_tranzila_direct_secret_word'] = array(
        '#type' => 'textfield',
        '#title' => t('Secret word for order verification'),
        '#description' => t('The secret word entered to authenticate the Tranzila Interface calls.'),
        '#default_value' => variable_get('uc_tranzila_direct_secret_word', 'tango'),
        '#size' => 16,
      );
      $form['uc_tranzila_direct_severe_failure_mail'] = array(
        '#type' => 'textfield',
        '#title' => t('Email address for severe failure notification'),
        '#description' => t('Email address to notify when severe failure happens(Payment is recieved but order is not completed due to invalid data recieved from Tranzila).'),
        '#default_value' => variable_get('uc_tranzila_direct_severe_failure_mail', variable_get('uc_store_email', '')),
        '#size' => 40,
      );
      
      return $form;
  }
}

/**
 * Implementation of hook_form_alter().
 */
function uc_tranzila_direct_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'tranzila_direct') {
      drupal_add_css(drupal_get_path('module', 'uc_tranzila_direct') . '/uc_tranzila_direct.css');
      unset($form['submit']);
      $form['#prefix'] = '<table id="tranzila-direct-review-table"><tr><td>';
      $form['#suffix'] = '</td><td>' . drupal_get_form('uc_tranzila_direct_form', $order) . '</td></tr></table>';
    }
  }
}

/**
 * Form to build the submission to tranzila.co.il.
 */
function uc_tranzila_direct_form($form_state, $order) {
  $country = uc_get_country_data(array('country_id' => $order->billing_country));
  if ($country === FALSE) {
    $country = array(0 => array('country_iso_code_3' => 'ISR'));
  }

  $context = array(
      'revision' => 'formatted-original',
      'type' => 'order_total',
      'subject' => array(
          'order' => $order,
      ),
  );
  $options = array(
      'sign' => FALSE,
      'dec' => '.',
      'thou' => FALSE,
  );
  $total = uc_price($order->order_total, $context, $options);
  
  // Phone info requires some format removal
  $phone = substr($order->billing_phone, 0, 16);
  // Tranzila should get the phone number without any dashes, format signs, etc.
  $phone = preg_replace("/[^0-9]/","", $phone);

  $data = array(
      'sum' => $total,
      'remarks' => $order->order_id, // TODO - change this to a better fit field when Tranzila will support that (currently this is inline with their direction)
      'contact' => substr($order->billing_first_name . ' ' . $order->billing_last_name, 0, 128),
      'address' => substr($order->billing_street1, 0, 64) . ' ' . substr($order->billing_street2, 0, 64),
      'city' => substr($order->billing_city, 0, 64),
      'email' => substr($order->primary_email, 0, 64),
      'phone' => $phone,
      'currency' => 1,
      'pdesc' => t('You have !num_of_products products in your cart', array('!num_of_products' => count($order->products))),
      'TranzilaToken' => md5($order->order_id.$order->order_total.variable_get('uc_tranzila_direct_secret_word', 'tango')),
  );

  $form['#action'] = 'https://direct.tranzila.com/' . variable_get('uc_tranzila_direct_terminal_name', '') . '/';

  foreach ($data as $name => $value) {
    $form[$name] = array('#type' => 'hidden', '#value' => $value);
  }

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => variable_get('uc_tranzila_direct_checkout_button', t('Submit Order')),
  );

  return $form;
}

/** Handles persumed successful completion by Tranzila
 *
 * @return type 
 */
function uc_tranzila_direct_complete() {

  // Data Initiation
  $order_id = $_POST['remarks'];
  $order = uc_order_load($order_id);
  $order_state = uc_order_status_data($order->order_status, 'state');
  $computed_token = md5($order->order_id.$order->order_total.variable_get('uc_tranzila_direct_secret_word', 'tango'));
  // Sending initiation update to watchdog
  $message = t('Receiving new order notification from Tranzila Direct for order !order_id.', array('!order_id' => check_plain($order_id)));
  watchdog('Tranzila Direct', $message);

  // Preparing the user message, in case failure occurs
  $messages['user'] = t("We're sorry.  An error occurred while processing your order that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible.");
  // First checking that the order id is ok.
  if (!isset($order_id) || $order === FALSE) {
    $messages['system'] = array( 
        t('Erroneous order recieved from Tranzila!'),
        t('Order id is not set or the Order is empty. Drupal-Cart order ID: @cart_order. Tranzila Order ID: !order_id', array('@cart_order' => $_SESSION['cart_order'], '!order_id' => $order_id)),
        t('Payment was made but the Order was not completed!'),
    );
    uc_tranzila_direct_failure_response($messages);
  // Check that the order is at the right state  
  } elseif ($order_state != 'in_checkout'){
    $messages['system'] = array(
        t('Order state is !order_state instead of in_checkout.', array('!order_state' => $order_state )),
        t('Payment was made but the Order was not completed!'),
    );    
    uc_tranzila_direct_failure_response($messages, $order_id);
  // Check that the response code and the total payment amount is correct
  } elseif ($_POST['Response'] != 0 || !is_numeric($_POST['sum'])) {
    $messages['system'] = array(
        t('Erroneous response code or total payment from Tranzila. Response code:!response.', array('!response' =>$_POST['Response'])),
        t('Further investigation on Tranzila should be made.'),
    );    
    uc_tranzila_direct_failure_response($messages, $order_id);
  //} elseif ($_POST['TranzilaToken'] != $computed_token) {
  }elseif(1==1){
    $messages['system'] = array(
        t('Attempted unverified access to order completion was blocked.'),
        t('Possibly, payment was made but the Order was not completed!'),
        t('This might be a fraud attempt.'),
    );    
    uc_tranzila_direct_failure_response($messages, $order_id, FALSE);
    drupal_access_denied();
  // Order was processed successfuly - now we should proceed with the order workflow.
  } else {
    // check here data inconsistency
    uc_tranzila_direct_is_data_modified($order_id,t('Order Id'),$_POST['remarks'],$order_id);
    uc_tranzila_direct_is_data_modified($order_id,t('Contact Name'),$_POST['contact'],substr($order->billing_first_name . ' ' . $order->billing_last_name, 0, 128));
    uc_tranzila_direct_is_data_modified($order_id,t('Address'),$_POST['address'],substr($order->billing_street1, 0, 64) . ' ' . substr($order->billing_street2, 0, 64));
    uc_tranzila_direct_is_data_modified($order_id,t('City'),$_POST['city'],substr($order->billing_city, 0, 64));
    uc_tranzila_direct_is_data_modified($order_id,t('Email'),$_POST['email'],substr($order->primary_email, 0, 64));
    uc_tranzila_direct_is_data_modified($order_id,t('Phone'),$_POST['phone'],substr($order->billing_phone, 0, 16));
    // Save changes to order. Must be done before entering payments or else status get overriden. Not needed for now.
    // uc_order_save($order);
    
    // Enter the payment into ubercart
    $comment = t('Paid by credit card, Tranzila Direct order #!order.', array('!order' => $order_id));
    uc_payment_enter($order_id, 'tranzila_direct', $_POST['sum'], 0, NULL, $comment);
    // Add a comment to let the team know payment was recieved successfuly.
    uc_order_comment_save($order->order_id, 0, t('Tranzila Direct payment completed successfuly. Order created.'), 'admin');

    // Finally complete the order.
    $output = uc_cart_complete_sale($order, variable_get('uc_new_customer_login', FALSE));
    watchdog('Tranzila Direct', 'order !order_id paid by Tranzila Direct, completed successfuly.', array('!order_id' => check_plain($order_id)));

    // Where do we go from here...
    $page = variable_get('uc_cart_checkout_complete_page', '');
    if (!empty($page)) {
      drupal_goto($page);
    }
    return $output;
  }
}

/** Check whether returned data was modified in anyway. If so indicate the store admin.
 *
 * @param type $order_id
 * @param type $field_name
 * @param type $tranzila_data
 * @param type $order_data 
 */
function uc_tranzila_direct_is_data_modified($order_id, $field_name, $tranzila_data, $order_data){
  if (trim($tranzila_data) != trim($order_data)){
    $msg_vars = array('!field_name' => $field_name, '!order_data' => $order_data, '!tranzila_data' => $tranzila_data);
    uc_order_comment_save($order_id, 0, t('The field !field_name was modified at the Tranzila page from !order_data to !tranzila_data.', $msg_vars), 'admin');
  }
}

/** Handles failure of Tranzila to process the payment (i.e. response code is not 0)
 * 
 */
function uc_tranzila_direct_failed() {
  $order_id = $_POST['remarks'];
  $response_code = $_POST['Response'];
  switch ($response_code) {
    // 004 - Credit Card refusal.
    case '004':
      $message = t("The payment was not authorized for the credit card provided.");
      break;
    // 006 - wrong CVV or ID.
    case '006':
      $message = t("The payment was not authorized due to invalid ID number and/or CVV number.");
      break;
    // 033 - Invalid credit card.
    case '033':
      $message = t("The payment was not authorized due to invalid credit card.");
      break;
    // 036 - Validity date passed. 
    case '036':
      $message = t("The credit card validity date has passed.");
      break;
    // 039 - Wrong credit card number. 
    case '039':
      $message = t("The credit card number was probably typed wrong.");
      break;
    // 001, 002 - Blocked or stolen credit card. 
    case '001':
    case '002':
      $message = t("The payment was not authorized for the credit card provided.");
      $watchdog_message = t("The payment was not authorized for the credit card provided. This is either blocked or stolen credit card!");
      break;
    // 003 - Merchant to call the Credit Card company (e.g. might be credit card limit reached).
    // or any other response code...
    default:
      $message = t("An error occurred while processing the order that prevents us from completing it at this time. Please contact us and we will resolve the issue as soon as possible.");
      break;
  }
  // Present the user message
  $messages['user'] = t("We're sorry.") . ' ' . $message . ' ' . t('This Order is not completed.');
  $watchdog_message = isset($watchdog_message) ? $watchdog_message : $message;
  
  $messages['system'] = array(
      t('Tranzila reported failure to process the payment.'),
      t('Tranzila response code: !response_code.', array('!response_code' => $response_code)),
      t('Related message: "!watchdog_message"', array('!watchdog_message' => $watchdog_message)),
      t('Order was not created. Drupal-Cart order ID: @cart_order. Tranzila Order ID: !order_id', array('@cart_order' => $_SESSION['cart_order'], '!order_id' => $order_id)),
      t('Tranzila transaction number: !index. Credit Card company confirmation code: !cc_confirm', array('!index' => $_POST['index'], '!cc_confirm' => $_POST['ConfirmationCode'])),
  );
  uc_tranzila_direct_failure_response($messages, $order_id, TRUE, FALSE);
}

/** On any failure handle message generation and necessary actions. This function is called both from the failure callback and the completion callback. 
 * When called from the completion callback this is a considered a severe failure and should be indicated as such using the relevant parameter.
 *
 * @param type $messages
 * @param type $order_id
 * @param type $goto
 * @param type $severe_failure
 */
function uc_tranzila_direct_failure_response($messages, $order_id = 0, $goto = TRUE, $severe_failure = TRUE){
  // Send user message to screen 
  if (isset($messages['user'])){
    drupal_set_message($messages['user'], 'error');
  }
  // Send system message to the admin comment of the order
  $system_message = implode('<br/>',$messages['system']);
  watchdog('Tranzila Direct', $system_message, null, WATCHDOG_ERROR);
  if (isset($order_id)){
    uc_order_comment_save($order_id, 0, $system_message, 'admin');
  }
  // In case of severe failure send mail (when Tranzila completed successfuly but Drupal failed the payment - see uc_tranzila_direct_complete)
  if ($severe_failure){
    $to = variable_get('uc_tranzila_direct_severe_failure_mail', variable_get('uc_store_email', ''));
    drupal_mail('uc_tranzila_direct','payment_recieved_order_failed',$to, language_default(), $system_message, variable_get('uc_store_email', ''), TRUE);
  }
  // Redirect to the checkout page (if needed)
  if ($goto) {
    drupal_goto('cart/checkout');
  }
}

/** implementation of hook_mail
 *
 * @param type $key
 * @param type $message
 * @param type $params 
 */
function uc_tranzila_direct_mail($key, &$message, $params){
  if ($key == 'payment_recieved_order_failed'){
    // mail subject 
    $message['subject'] = t('Severe payment failure at !store', array('!store' => variable_get('uc_store_name', '')));
    // mail body
    $mail_message = array(
        t('Severe payment processing failure occured.'),
        t('Tranzila reported on successufl payment but the details recieved from Tranzila are invalid thus order processing stopped.'),
        t('Possible fraud attempt.'),
        t('See below the generated system message for further details:'),
        $params,
        t('This order requires further manual handling.')
    );
    $message['body'][] = implode('<br/>',$mail_message);
    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
  }
}

